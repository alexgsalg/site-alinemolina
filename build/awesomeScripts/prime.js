
//SLICK SLIDER
// usar documentação http://kenwheeler.github.io/slick/
$(document).ready(function(){
  $('.your-class').slick({
    // setting-name: setting-value
  });
});

//STELLAR PARALLAX
$(window).stellar();


// PHONE MASK

    //COLAR ANTE DO </BODY> <script src="https://igorescobar.github.io/jQuery-Mask-Plugin/js/jquery.mask.min.js"></script>
  var SPMaskBehavior = function (val) {
  return val.replace(/\D/g, '').length === 11 ? '(00) 00000-0000' : '(00) 0000-00009';
    },
  spOptions = {
    onKeyPress: function(val, e, field, options) {
        field.mask(SPMaskBehavior.apply({}, arguments), options);
      }
  };
    $('.phone-ddd').mask(SPMaskBehavior, spOptions);
