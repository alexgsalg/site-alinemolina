/*==================
    Phone Mask
==================*/
    var SPMaskBehavior = function (val) {
        return val.replace(/\D/g, '').length === 11 ? '(00) 00000-0000' : '(00) 0000-00009';
          },
        spOptions = {
          onKeyPress: function(val, e, field, options) {
              field.mask(SPMaskBehavior.apply({}, arguments), options);
            }
        };
          $('.phone-ddd').mask(SPMaskBehavior, spOptions);
      
/*==================
  Navbar scroll BG
==================*/
// $(document).ready(function(){       
//     var scroll_start = 95;
//     var atuacao = $('#atuacao');
//     var offset = atuacao.offset();
//         if (atuacao.length){
//             $(document).scroll(function() { 
//                 scroll_start = $(this).scrollTop();
//                 if(scroll_start > offset.top - 100) {
//                     $(".navbar").css('background-color', '#252D64');
//                 } else {
//                     $('.navbar').css('background-color', 'transparent');
//                 }
//             });
//         }
// });


/*==================
    Slick Slider
==================*/
/* Tesiemonial */
$('.testemonial-container').slick({
    prevArrow: $(".arrowLeft"),
    nextArrow: $(".arrowRight")
});

/* Gallery */
$('.case-gallery').slick({
    prevArrow: $(".imgLeft"),
    nextArrow: $(".imgRight")
});

/* 3D */
$('.case-dimension').slick({
    prevArrow: $(".dimensionLeft"),
    nextArrow: $(".dimensionRight")
});


/*==================
   Hamburguer Menu
==================*/

$('.bt-menu').on('click', function(e) {
    e.preventDefault();
    $('body').toggleClass('menu-open');
  });


/*==================
    Smooth Scroll
==================*/

 // Select all links with hashes
$('a[href*="#"]')
  // Remove links that don't actually link to anything
  .not('[href="#"]')
  .not('[href="#0"]')
  .click(function(event) {
    // On-page links
    if (
      location.pathname.replace(/^\//, '') == this.pathname.replace(/^\//, '') 
      && 
      location.hostname == this.hostname
    ) {
      // Figure out element to scroll to
      var target = $(this.hash);
      target = target.length ? target : $('[name=' + this.hash.slice(1) + ']');
      // Does a scroll target exist?
      if (target.length) {
        // Only prevent default if animation is actually gonna happen
        event.preventDefault();
        $('html, body').animate({
          scrollTop: target.offset().top
        }, 1000, function() {
          // Callback after animation
          // Must change focus!
          var $target = $(target);
          $target.focus();
          if ($target.is(":focus")) { // Checking if the target was focused
            return false;
          } else {
            $target.attr('tabindex','-1'); // Adding tabindex for elements not focusable
            $target.focus(); // Set focus again
          };
        });
      }
    }
  });


/*=====================
Navbar active w/Scroll
=====================*/

$(document).ready(function () {
    $(document).on("scroll", onScroll);

    $('a[href^="#"]').on('click', function (e) {
        e.preventDefault();
        $(document).off("scroll");

        $('a').each(function () {
            $(this).removeClass('active');
        })
        $(this).addClass('active');

        var target = this.hash;
        $target = $(target);
        $('html, body').stop().animate({
            'scrollTop': $target.offset().top+2
        }, 500, 'swing', function () {
            window.location.hash = target;
            $(document).on("scroll", onScroll);
        });
    });
});

function onScroll(event){
    var scrollPosition = $(document).scrollTop();
    $('nav a').each(function () {
        var currentLink = $(this);
        var refElement = $(currentLink.attr("href"));
        if (refElement.position().top <= scrollPosition && refElement.position().top + refElement.height() > scrollPosition) {
            $('nav ul li a').removeClass("active");
            currentLink.addClass("active");
        }
        else{
            currentLink.removeClass("active");
        }
    });
}



/*=====================
Filtro de portfolio
=====================*/
